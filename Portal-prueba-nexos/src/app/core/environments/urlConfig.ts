/*
 * Redcolsa Colombiana De Servicios S.A
 * Copyright 2020-2030 Redcolsa, Todos los derechos reservados
 */


 /** 
 * @author:Smarthink Consulting Group S.A.S
 * @description: Clase encargada de gestionar la logica de carga del archivo
 **/
export class Configure {
    private static config: any = null;
    private static status: number;
    public static IpPortZuul: any = null;
    public static IpLogin: any = null;

    public static init() {
        this.loadConfigure('./assets/constantes/urlConfig.json');
        this.IpLogin = `${this.config.IPZUUL}:${this.config.PORTZUUL}/microservicio-back-transaccional-login/facelectapi/login/`;
    }

    public static loadConfigure(filePath: string): void {
        const request = new XMLHttpRequest();
        request.open('GET', filePath, false);
        request.send();
        if (request.status === 200) {
            this.config = JSON.parse(request.responseText);
            this.IpPortZuul = `${this.config.IPZUUL}:${this.config.PORTZUUL}`;
        }
        this.status = request.status;
    }

  

    public static getVersion(): string {
        this.loadConfigure('../assets/constant/url-config.json');
        return this.config.VERSION_PORTAL;
    }

    public static getUrl():string{ 
        this.loadConfigure('../assets/constant/url-config.json');
        return `${this.config.URL}`;
    }
 
 


}
