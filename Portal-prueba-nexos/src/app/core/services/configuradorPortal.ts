import { Injectable, OnInit, Component, Inject, ViewChild } from '@angular/core';
import Swal from "sweetalert2";
import { Location } from "@angular/common";
import { Router, NavigationStart } from "@angular/router";
import * as CryptoJS from "crypto-js";
import { Configure } from '../environments/urlConfig';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
declare var $: any;


 /** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar las utilidades y metodos genericos.
 **/
@Injectable()
export class Configurador implements OnInit {
  estado: string ="";
  dialogRef: any;
  timeOutFocus: any;

  producto = "3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55";
  loteria :string= "F27D5C9927726BCEFE7510B1BDD3D137";
  apuesta = "&M4RT_1NK20JAN21*";
  iterations = 10000;
  parameto = 128/32;
  datosEmpresa: any;
  

  
  constructor(
    private _location: Location,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

   }

         /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de generar la llave para la encriptacion
 **/
  generarLlave():any{
    const KEY = CryptoJS.PBKDF2(
      this.apuesta,
      CryptoJS.enc.Hex.parse(this.producto),
      { keySize: this.parameto, iterations: this.iterations }
    );
    return KEY;
  }

        /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de encriptar cadenas
 * @param: plainText cadena la cual se desea encriptar
 **/
  encriptarTrama = function (this:any,plainText:any) {
    const KEY = this.generarLlave(this.producto, this.apuesta);
    //console.log('KEY', KEY);
    const ENCRYPTED = CryptoJS.AES.encrypt(plainText, KEY, {
      iv: CryptoJS.enc.Hex.parse(this.loteria)
    });
    return ENCRYPTED.toString();
  };


        /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de desencriptar cadenas
 * @param: cipherText es la informacion cifrada la cual deseo desencriptar
 **/
  desencriptarTrama = function (this:any,cipherText:any) {
    const KEY = this.generarLlave(this.producto, this.apuesta);
    const DECRYPTED = CryptoJS.AES.decrypt(cipherText, KEY, {
      iv: CryptoJS.enc.Hex.parse(this.loteria)
    });
    return DECRYPTED.toString(CryptoJS.enc.Utf8);
  };


      /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de limpiar el formulario
 * @param: nombre me indica el nombre de la variable que deseo guardar en el local storage
 *         datos informacion que deseo guardar
 **/
  setLocal(nombre: string, datos: string) {
    localStorage.setItem(nombre, this.encriptarTrama(datos));
  }

        /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de limpiar el formulario
 * @param: nombre me indica el nombre de la variable que deseo obtener de el local storage
 **/
  getLocal(nombre: string) {
    return this.desencriptarTrama(localStorage.getItem(nombre));
  }

        /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de limpiar el local storage
 **/
  clearLocal() {
    localStorage.clear();
  }

        /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de desplazar el scroll
 * @param: element me indica la orientacion hacia donde se dirijira -
 *         opcional cantidad de pixeles que desea desplazar
 **/
  scroll(element:any, opcional?:any) {
    if (!opcional) {
      opcional = 0;
    }

    if (element === "up") {
      $("html, body").animate({ scrollTop: "0px" }, 700);
    } else {
      setTimeout(function () {
        if ($(element).length > 0) {
          let posicionBoton = $(element).offset().top - opcional;
          $("html, body").animate({ scrollTop: posicionBoton + "px" }, 1000);
        }
      }, 200);
    }
  }


          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de desplegar ventana emergente
 * @param: titulo titulo de la ventana - tam indica el tamaño de la ventana- indica si es una ventana de confirmacion
 *         mensaje de la ventana emergente
 **/
  ventanaEmergente(titulo:any, mensaje:any, tam?:any, confirmar?:any, focus?:any) {
    $("#bloqPantalla").fadeOut();

    if (!tam || tam === "not") {
      tam = "32rem";
    }

    if (!confirmar || confirmar === "not") {
      confirmar = "OK";
    }

    Swal({
      title: titulo,
      html: mensaje,
      confirmButtonText: confirmar,
      customClass: "estiloModales",
      confirmButtonColor: "#0069c0",
      width: tam
    }).then(result => {
      $("#loader").fadeOut(1000);
      if (focus) {
        setTimeout(() => $(focus).focus(), 200);
      }
      return false;
    });
  }

            /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de hacer focus en determinado componente
 * @param: id me indica el nombre del componente al cual deseo hacerle focus
 **/
  focus(id:any) {
    clearTimeout(this.timeOutFocus);
    this.timeOutFocus = setTimeout(() => {
      if ($(".swal2-shown").length === 0) {
        $(id).focus();
      }
    }, 200);
  }

              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de ir a la ruta anterior
 **/
  volverAtras() {
    this._location.back();
  }


              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de dirigirme a una ruta predeterminada
 * @param: ruta me indica la ruta a la cual deseo dirigirme 
 **/
  ir(ruta: string[]) {
    this.router.navigate(ruta);
  }

              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de desplegar el loarder
 **/
  loader() {
    this.dialogRef = this.dialog.open(ModalLoaderComponent, {
      width: "250px",
      disableClose: true
    });
  }


                /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de cerrar el loarder
 **/
  closeModal() {
    if (this.dialogRef && $(".mat-dialog-container").length > 0) {
      this.dialogRef.close();
    }
  }

              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de darle formato a un numero
 * @param: numero es el numero al cual deseo darle el formato
 **/
  numeroFormato(numero:any) {
    numero = numero.toString();
    let numeroArray = numero.split("");
    let numeroTemp;
    if (numeroArray.length === 10) {
      numeroTemp =
        numero[0] +
        numero[1] +
        numero[2] +
        "-" +
        numero[3] +
        numero[4] +
        numero[5] +
        "-" +
        numero[6] +
        numero[7] +
        numero[8] +
        numero[9];
    } else {
      numeroTemp = numero;
    }
    return numeroTemp;
  }



              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de darle formato a una fecha
 * @param: fecha me indica la fecha a la cual deseo darle el formato
 **/
  fechaFormato(fecha: Date) {
    let dia: any = fecha.getDate();
    if (dia.toString().length === 1) {
      dia = "0" + dia;
    }
    let mes: any = fecha.getMonth() + 1;
    if (mes.toString().length === 1) {
      mes = "0" + mes;
    }
    let año: number = fecha.getFullYear();
    return dia + "/" + mes + "/" + año;
  }


              /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de validarme lo que escribo sobre un input
 * @param: tipo me indica el tipo de validacion que deseo aplicar
 **/
  public validarCaracter(e: any, tipo: string) {
    let tecla = document.all ? e.keyCode : e.which;
    // Tecla de retroceso para borrar, siempre la permite
    if (tecla === 8) {
      return true;
    }

    let patron:any;
    switch (tipo) {
      case "ALF":
        patron = /[A-Za-z0-9]/;
        break;
      case "NUM":
        patron = /[0-9]/;
        break;
      case "SERV":
        patron = /[0-9.]/;
        break;
      case "SER":
        patron = /[0-9.-]/;
        break;
      case "TEX":
        patron = /[A-Za-z]/;
        break;
      case "DIR": // para direcciones
        patron = /[A-Za-z0-9#°\- ]/;
        break;
      case "RAZ": // para razon social ya que permite punto
        patron = /[A-Za-z0-9. ]/;
        break;
      case "PAS": // se valida que la clave contenga caracteres validos configurados en bd
        patron = /[A-Za-z0-9\\/:*=-¡!.]/;
        break;
      case "ESP": // texto con espacio
        patron = /[A-Za-z ]/;
        break;
      case "DECI":
        patron = /[0-9.]/;
        break;
      case "DECI2":
        patron = /[0-9.-]/;
        break;
    }
    return patron.test(String.fromCharCode(tecla));
  }


  bloquearPantalla() {
    $("#bloqPantalla").fadeIn();
  }

  desbloquearPantalla() {
    $("#bloqPantalla").fadeOut();
  }
}


@Component({
  selector: "app-modal-loader",
  templateUrl: "../layout/loader/loader.component.html",
  styleUrls: ["../layout/loader/loader.component.css"]
})

 /** 
 * @author:Henry Garcia
 * @description: Clase encargada del modal del loader.
 **/
export class ModalLoaderComponent implements OnInit {
  configuracion: any;
  color:any="red";
  tipoFuente:any;
  tamanoFuente:any;

  constructor(public dialogRef: MatDialogRef<ModalLoaderComponent>) 
  {


   }

  ngOnInit() {
    $("#loaderContenedor").css({
      position: "relative",
      top: 0,
      left: 0,
      height: "250px"
    });

    $("#loader").css({
      height: "250px"
    });
    $("#loader").fadeIn();
    $(".lds-spinner div:after").css({
      background:this.color
    })
  }
}







