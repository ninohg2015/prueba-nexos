import { AuthService } from 'ng2-ui-auth';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Configurador } from './configuradorPortal';


@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private auth: AuthService,public confi:Configurador) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.auth.isAuthenticated()) {
            //console.log('No estás logueado');
            this.confi.ir(['/']);
            return false;
        } 
        return true;
    }
}
