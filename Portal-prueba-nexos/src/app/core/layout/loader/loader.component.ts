


import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})


export class LoaderComponent implements OnInit {

  coloresLoader: object;

  constructor() { }

  ngOnInit() {
    this.coloresLoader = { 'border-color': '#333 transparent' };
    setTimeout(() => {
      let tamH = $('#loader').parent().parent().parent().css('height');
      $('#loader').css('height', tamH);
    }, 100);

    $(window).resize(() => {
      let id: any;
      clearTimeout(id);
      id = setTimeout(() => {
        let tamH = $('#loader').parent().parent().parent().css('height');
        $('#loader').css('height', tamH);
      });
    });
  }

}
