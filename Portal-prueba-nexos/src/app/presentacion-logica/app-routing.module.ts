import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuardService } from '../core/services/authGuard.service';

const routes: Routes = [
  { path: '', loadChildren: () =>
    import('../components/inicio/presentacion-logica/inicio.module').then(m => m.InicioModule),canActivate:[AuthGuardService] 
  },  { path: 'mercancia', loadChildren: () =>
import('../components/mercancia/presentacion-logica/mercancia.module').then(m => m.MercanciaModule),canActivate:[AuthGuardService] 
},  { path: 'home', loadChildren: () =>
import('../components/home/presentacion-logica/home.module').then(m => m.HomeModule),canActivate:[AuthGuardService] 
},  { path: 'usuario', loadChildren: () =>
import('../components/usuario/presentacion-logica/usuario.module').then(m => m.UsuarioModule),canActivate:[AuthGuardService] 
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
