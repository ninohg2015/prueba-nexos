import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component } from '@angular/core';
import { AuthService } from 'ng2-ui-auth';
import { Configure } from '../core/environments/urlConfig';
import { Configurador } from '../core/services/configuradorPortal';

@Component({
    selector: 'app-inicio',
    templateUrl: '../presentacion/app.component.html',
  styleUrls: [  '../presentacion/app.component.css']
})

export class appComponent {
  title = 'portal-mercancia';
  isLogin=false;
  location='';
  version='';
  constructor(private auth: AuthService,public confi:Configurador){
      let intervalo = setInterval(() => {
        this.isLogin = this.auth.isAuthenticated();
      },999);
      this.version=Configure.getVersion();
  }


  cerrarSesion(){
    this.confi.loader();
    localStorage.clear();
    sessionStorage.clear();
    setTimeout(() => {
      this.confi.ir(['/']);
      this.confi.closeModal();
    }, 1000);

  }
}
