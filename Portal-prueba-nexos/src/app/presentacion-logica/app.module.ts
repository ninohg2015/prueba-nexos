import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InicioModule } from '../components/inicio/presentacion-logica/inicio.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { CustomMaterialModule } from '../core/material-module';
import { AuthService, Ng2UiAuthModule } from 'ng2-ui-auth';
import { AuthGuardService } from '../core/services/authGuard.service';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { Configurador } from '../core/services/configuradorPortal';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MAT_COLOR_FORMATS, NgxMatColorPickerModule, NGX_MAT_COLOR_FORMATS } from '@angular-material-components/color-picker';
import { MatToolbarModule } from '@angular/material/toolbar';
import { appComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from '../components/home/presentacion-logica/home.module';
import { Configure } from '../core/environments/urlConfig';
import { MercanciaModule } from '../components/mercancia/presentacion-logica/mercancia.module';

let DEFAULT_OPTIONS = {
  loginUrl: `${Configure.getUrl()}/api/usuario/login`,
  tokenName: 'token',
  tokenPrefix: 'entity',
  oauthType: '2.0'
};

@NgModule({
  declarations: [
    appComponent
  ],
  entryComponents:[
    appComponent
  ]
  ,
  imports: [
    BrowserModule,
    InicioModule,
    HomeModule,
    NoopAnimationsModule,
    MercanciaModule,
    RouterModule,
    AppRoutingModule,
    Ng2UiAuthModule.forRoot(DEFAULT_OPTIONS),
    AmazingTimePickerModule,
    NgxMatColorPickerModule,
    CustomMaterialModule
  ],
  providers: [
    AuthService,
    Configurador,
    { provide: MAT_DATE_LOCALE, useValue: "es-CO" },
    { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS },
    AuthGuardService
  ],
  bootstrap: [appComponent]
})
export class AppModule { }
