import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "ng2-ui-auth";
import { CustomMaterialModule } from "src/app/core/material-module";
import { UsuarioComponent } from "./usuario.component";
import { UsuarioRoutingModule } from "./usuario.routing.module";
import { UsuarioService } from "./usuario.service";

@NgModule({
  declarations: [
    UsuarioComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    UsuarioRoutingModule,
    HttpClientModule
  ],
  providers: [
    UsuarioService
  ],
  bootstrap: [UsuarioComponent],
  exports: [
    UsuarioComponent
  ]
})
export class UsuarioModule { }
