
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Configurador } from 'src/app/core/services/configuradorPortal';
import { Configure } from 'src/app/core/environments/urlConfig';
import { CargoDto } from '../../model/CargoDto';
import { UsuarioDto } from '../../model/UsuarioDto';

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar los servicios de usuarios
 **/

@Injectable()
export class UsuarioService {


  constructor(
    private http: HttpClient, public confi:Configurador
  ) { }

          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de consultar los usuarios
 * @param: no tiene parametros
 **/
getUsuarios():Observable<any>  {

  return this.http.post(`${Configure.getUrl()}/api/consultar/consultarUsuarios`,
  null).pipe(map((response) => response, (error:any) => error));
}


          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de consultar los cargos
 * @param: no tiene parametros
 **/
getCargos():Observable<any>  {
  return this.http.post(`${Configure.getUrl()}/api/consultar/obtenerCargos`,
  null).pipe(map((response) => response, (error:any) => error));
}


         /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de guardar el producto
* @param: usuario este es el objeto del usuario que se desea guardar
 **/
guardarUsuario(usuario:UsuarioDto):Observable<any>  {
  return this.http.post(`${Configure.getUrl()}/api/usuario/crearUsuario`,
  usuario).pipe(map((response) => response, (error:any) => error));
}


         /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de eliminar el producto
* @param: usuario este es el objeto del usuario que se desea eliminar
 **/
eliminarUsuario(usuario:UsuarioDto):Observable<any>  {
  return this.http.post(`${Configure.getUrl()}/api/usuario/eliminarUsuario`,
  usuario).pipe(map((response) => response, (error:any) => error));
}



}
