import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'ng2-ui-auth';
import { Configurador } from 'src/app/core/services/configuradorPortal';
import { UsuarioService } from './usuario.service';
import { MatTableDataSource } from '@angular/material/table';
import { UsuarioDto } from '../../model/UsuarioDto';
import { GenericDto } from '../../model/GenericDto';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CargoDto } from '../../model/CargoDto';

@Component({
  selector: 'usuario-app',
  templateUrl: '../presentacion/usuario.component.html',
  styleUrls: ['../presentacion/usuario.component.css']
})

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar la logica de usuarios
 **/
export class UsuarioComponent {
  hideConfirmPas = false;
  hideNewPass = false;
  noLoading = true;
  isRoot = false;
  userid: string = "";
  userdoc = '';
  userpass = '';
  formUsuario: FormGroup;
  isEditarUsuario = false;
  usuarios: Array<UsuarioDto> = [];
  cargos: Array<CargoDto> = [];
  dataSource: any;
  displayedColumns: string[] = [
    "id",
    "nombre",
    "fecha_ingreso",
    "edad",
    "opciones"
  ];
  idUsuario: number;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private auth: AuthService, public confi: Configurador, private usuarioService: UsuarioService) {
    this.crearControlesUsuario();
    this.dataSource = new MatTableDataSource(this.usuarios);
    setTimeout(() => {
      this.dataSource.paginator = this.usuarios;
    }, 700);
    this.obtenerUsuarios(true);
    this.obtenerCargos();

  }

  crearControlesUsuario() {
    this.formUsuario = new FormGroup({
      usuario: new FormControl("", Validators.required),
      nuevaPassword: new FormControl("", Validators.required),
      confirmarPassword: new FormControl("", Validators.required),
      edad: new FormControl("", Validators.required),
      cargo: new FormControl("", Validators.required),
      fechaIngreso: new FormControl("", Validators.required)
    })
  }

  /** 
* @author:Henry Garcia
* @description: Funcion encargada de limpiar el formulario
* @param: isLoader me indica si se debe o no desplegar el loader al llamar la consulta de usuario -
*         formUsuario este es unobjeto de tipo formulario
**/
  limpiarForma(formUsuario: FormGroup) {
    formUsuario.reset();
    this.obtenerUsuarios(false);
    this.isEditarUsuario = false;
    this.idUsuario = 0;
  }



  /** 
* @author:Henry Garcia
* @description: Funcion encargada de obtener usuarios
* @param: no tiene parametros
**/
  obtenerUsuarios(loader: boolean) {

    this.usuarios = [];
    if (loader) {
      this.confi.loader();
    }
    setTimeout(() => {
      this.usuarioService.getUsuarios().subscribe((rs: GenericDto) => {
        if (rs.status !== "500") {
          console.log('rs', rs);
          this.usuarios = rs.payload;
          this.dataSource = new MatTableDataSource(this.usuarios);
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
          }, 700);
        } else {
          this.confi.closeModal();
          this.confi.ventanaEmergente("Usuario", "Señor usuario, " + rs.payload);
        }

      }, (error: any) => {
        this.isEditarUsuario = false;
        this.confi.closeModal();
        this.confi.ventanaEmergente("Usuario", "Señor usuario, ha ocurrido un error al consultar productos por favor ingrese mas tarde nuevamente.");
        this.confi.ir(['/home'])
      }
      )
    }, 700);

  }

  /** 
* @author:Henry Garcia
* @description: Funcion encargada de obtener los cargos
* @param: no tiene parametros
**/
  obtenerCargos() {

    this.usuarios = [];
    setTimeout(() => {
      this.usuarioService.getCargos().subscribe((rs: GenericDto) => {
        if (rs.status !== "500") {
          this.cargos = rs.payload;
          console.log('this.cargos', this.cargos);
          this.confi.closeModal();
          this.formUsuario.reset();
        } else {
          this.confi.closeModal();
          this.confi.ventanaEmergente("Usuario", "Señor usuario, " + rs.payload);
        }

      }, (error: any) => {
        this.isEditarUsuario = false;
        this.confi.closeModal();
        this.confi.ventanaEmergente("Usuario", "Señor usuario, ha ocurrido un error al consultar productos por favor ingrese mas tarde nuevamente.");
        this.confi.ir(['/home'])
      }
      )
    }, 1400);


  }


  /** 
* @author:Henry Garcia
* @description: Funcion encargada de guardar usuario
* @param: formUsuario el cual se usa para limpiar el formulario
**/
  guardarUsuario(formUsuario: FormGroup) {
    this.confi.loader();

    if (!this.formUsuario.valid) {
      this.confi.ventanaEmergente("Usuario", "Señor usuario, por favor ingrese los campos obligatorios.");
      this.confi.closeModal();
    } else {
      if (this.validarContraseña()) {


        setTimeout(() => {
          let usuario: UsuarioDto = this.obtenerObjetoUsuario();
          console.log('usuario', usuario);
          this.usuarioService.guardarUsuario(usuario).subscribe((rs: GenericDto) => {
            if (rs.status !== "500") {
              console.log('rs', rs);
              this.confi.closeModal();
              this.confi.ventanaEmergente("Usuario", "Señor usuario, " + rs.payload);
              this.limpiarForma(formUsuario);
            } else {
              this.confi.closeModal();
              this.confi.ventanaEmergente("Usuario", "Señor usuario, " + rs.payload);
            }

          }, (error: any) => {
            this.isEditarUsuario = false;
            this.confi.closeModal();
            this.confi.ventanaEmergente("Usuario", "Señor usuario, ha ocurrido un error al consultar productos por favor ingrese mas tarde nuevamente.");
            this.confi.ir(['/home'])
          }
          )
        }, 700);
      }
    }

  }


    /** 
* @author:Henry Garcia
* @description: Funcion encargada de validar los campos de contraseña
* @param: no tiene parametros
**/
  validarContraseña(): boolean {
    if (this.formUsuario.controls.nuevaPassword.value == this.formUsuario.controls.confirmarPassword.value) {
      return true;
    }
    this.confi.ventanaEmergente("Usuario", "Señor usuario, las contraseñas ingresadas no coinciden.");
    return false;
  }


    /** 
* @author:Henry Garcia
* @description: Funcion encargada de obtener los valores del objeto usuario
* @param: no tiene parametros
**/
  obtenerObjetoUsuario(): UsuarioDto {
    let usuario: UsuarioDto = {
      cargo: this.formUsuario.controls.cargo.value,
      edad: this.formUsuario.controls.edad.value,
      password: this.formUsuario.controls.nuevaPassword.value,
      fecha_ingreso: this.formUsuario.controls.fechaIngreso.value,
      nombre: this.formUsuario.controls.usuario.value,
      id: this.idUsuario != 0 ? this.idUsuario : 0,
    }
    return usuario;
  }

    /** 
* @author:Henry Garcia
* @description: Funcion encargada de eliminar usuario
* @param: usuario este es el objeto del usuario que se desea eliminar
**/
  eliminarUsuario(usuario: UsuarioDto) {
    console.log('usuario', usuario);
    this.confi.loader();
    setTimeout(() => {
      this.usuarioService.eliminarUsuario(usuario).subscribe(
        (respuesta: GenericDto) => {
          this.confi.closeModal();
          if (respuesta.status !== "500") {
            this.confi.ventanaEmergente("Usuario", "Señor usuario, " + respuesta.payload);
            this.obtenerUsuarios(false);
          } else {
            this.confi.ventanaEmergente("Usuario", "Señor usuario, " + respuesta.payload);
          }
        }, (error: any) => {
          console.log('error', error);
          this.confi.closeModal();
          this.confi.ventanaEmergente("Usuario", "Señor usuario, ha ocurrido un error al eliminar producto.");
        }
      )
    }, 700);

  }

     /** 
* @author:Henry Garcia
* @description: Funcion encargada de listar usuario
* @param: usuario este es el objeto del usuario que se desea listar y formUsuario el cual se usa para limpiar el formulario
**/
  listarUsuario(usuario: UsuarioDto, formUsuario: FormGroup) {
    this.confi.loader();
    setTimeout(() => {
      this.confi.closeModal();
      formUsuario.reset();
      this.formUsuario.controls.cargo.setValue(usuario.cargo);
      this.formUsuario.controls.edad.setValue(usuario.edad);
      this.formUsuario.controls.fechaIngreso.setValue(usuario.fecha_ingreso);
      this.formUsuario.controls.usuario.setValue(usuario.nombre);
      this.idUsuario = usuario.id;
      this.isEditarUsuario = true;
    }, 700);


  }


}
