import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Configurador } from 'src/app/core/services/configuradorPortal';
import { Configure } from 'src/app/core/environments/urlConfig';
import { GenericDto } from '../../model/GenericDto';
import { ConsultaDto } from '../../model/ConsultaDto';
import { ProductosDto } from '../../model/ProductoDto';

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar los servicios de mercancia
 **/

@Injectable()
export class MercanciaService {


  constructor(
    private http: HttpClient, public confi:Configurador
  ) { }


          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de consultar los productos
 * @param: no tiene parametros
 **/
  getProductos(consulta:ConsultaDto):Observable<any>  {

    return this.http.post(`${Configure.getUrl()}/api/consultar/consultarProductosFiltro`,
    consulta)
        .pipe(map((response) => response, (error:any) => error));
}


          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de guardar producto
  * @param: producto este es el objeto a guardar
 **/
 guardarProducto(producto:ProductosDto):Observable<any>  {

  return this.http.post(`${Configure.getUrl()}/api/producto/crearProducto`,
  producto).pipe(map((response) => response, (error:any) => error));
}


          /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de eliminar producto
  * @param: producto este es el objeto a eliminar
 **/
eliminarProducto(producto:ProductosDto):Observable<any>  {

  return this.http.post(`${Configure.getUrl()}/api/producto/eliminarProducto`,
  producto).pipe(map((response) => response, (error:any) => error));
}


 


}
