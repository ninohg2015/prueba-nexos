import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "ng2-ui-auth";
import { CustomMaterialModule } from "src/app/core/material-module";
import { EmpresaRoutingModule } from "./mercancia.routing.module";
import { MercanciaService } from "./mercancia.service";
import { AmazingTimePickerService } from 'amazing-time-picker';
import { MAT_COLOR_FORMATS, NgxMatColorPickerModule, NGX_MAT_COLOR_FORMATS } from '@angular-material-components/color-picker';
import { MercanciaComponent } from './mercancia.component';

@NgModule({
  declarations: [
    MercanciaComponent
  ],
  entryComponents:[
    MercanciaComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    EmpresaRoutingModule,
    HttpClientModule,
    NgxMatColorPickerModule
  ],
  providers: [
    MercanciaService,
    AmazingTimePickerService,
    { provide: MAT_COLOR_FORMATS, useValue: NGX_MAT_COLOR_FORMATS }

  ],
  bootstrap: [MercanciaComponent],
  exports: [
    MercanciaComponent
  ]
})
export class MercanciaModule { }
