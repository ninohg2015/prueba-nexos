import { ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'ng2-ui-auth';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Configurador } from 'src/app/core/services/configuradorPortal';
import { MercanciaService } from './mercancia.service';
import { MatDialog } from '@angular/material/dialog';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { GenericDto } from '../../model/GenericDto';
import { ProductosDto } from '../../model/ProductoDto';
import { ConsultaDto } from '../../model/ConsultaDto';
declare var $: any;

@Component({
  selector: 'mercancia-app',
  templateUrl: '../presentacion/mercancia.component.html',
  styleUrls: ['../presentacion/mercancia.component.css']
})

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar la logica de mercancia
 **/
export class MercanciaComponent {

  displayedColumns: string[] = [
    "id",
    "cantidad",
    "fecha_ingreso",
    "nombre",
    "opciones"
  ];
  formProducto = new FormGroup({});
  dataSource: any;
  productos: Array<ProductosDto>;
  isEditarProducto = false;
  usuario: any;
  idProducto:number;

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private atp: AmazingTimePickerService, public confi: Configurador, private productoService: MercanciaService,
    public dialog: MatDialog) {
    this.crearControlesProducto();
    this.usuario = parseInt(this.confi.getLocal("idUsuario"));
    this.dataSource = new MatTableDataSource(this.productos);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 700);
  }

  /** 
* @author:Henry Garcia
* @description: Funcion encargada de crear los controles del producto
**/
  crearControlesProducto() {
    this.formProducto = new FormGroup({
      cantidad: new FormControl("", Validators.required),
      nombre: new FormControl("", Validators.required),
      fechaIngreso: new FormControl("", Validators.required)
    })

  }



  /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de obtener los productos
  * @param: no tiene parametros
  **/
  obtenerProductos() {
    if (this.validarProductos()) {
      const consulta: ConsultaDto = {
        fecha_ingreso: this.formProducto.controls.fechaIngreso.value,
        nombre: this.formProducto.controls.nombre.value
      }
      this.productos = [];
      this.confi.loader();
      setTimeout(() => {
        this.productoService.getProductos(consulta).subscribe((rs: GenericDto) => {
          console.log('rs', rs);
          this.productos = rs.payload;
          console.log('this.productos', this.productos);
          this.dataSource = new MatTableDataSource(this.productos);
          setTimeout(() => {
            this.dataSource.paginator = this.paginator;
          }, 700);
          this.confi.closeModal();
        }, (error: any) => {
          this.isEditarProducto = false;
          this.confi.closeModal();
          this.confi.ventanaEmergente("Mercancia", "Señor usuario, ha ocurrido un error al consultar productos por favor ingrese mas tarde nuevamente.");
          this.confi.ir(['/home'])
        }
        )
      }, 700);

    }
  }

    /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de obtener el objeto producto
  * @param: no tiene parametros
  **/
  obtenerObjetoProducto():ProductosDto{
    let producto:ProductosDto={
      cantidad:this.formProducto.controls.cantidad.value,
      id:this.idProducto != undefined ?this.idProducto:0,
      usuario:this.usuario,
      fecha_ingreso:this.formProducto.controls.fechaIngreso.value,
      nombre:this.formProducto.controls.nombre.value
    }
    return producto;
  }


      /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de guardar producto
  * @param: formProducto el cual se usa para limpiar el formulario
  **/
  guardarProducto(formProducto:FormGroup){
    this.confi.loader();
    if (this.formProducto.valid) {
      setTimeout(() => {
        if (this.formProducto.controls.fechaIngreso.value > new Date()) {
          this.confi.closeModal();
          this.confi.ventanaEmergente("Mercancia", "Señor usuario, la fecha de ingreso no puede ser superior a la fecha actual.");
        } else{
        let producto = this.obtenerObjetoProducto();
        console.log('producto', producto);
        this.productoService.guardarProducto(producto).subscribe(
          (respuesta:GenericDto)=>{
            this.confi.closeModal();
            if (respuesta.status !== "500") {
              this.limpiarForma(formProducto);
              this.confi.ventanaEmergente("Mercancia", "Señor usuario, "+respuesta.payload);     
            } else{
              this.confi.ventanaEmergente("Mercancia", "Señor usuario, "+respuesta.payload);     
            }
          }, (error: any) =>{
            console.log('error', error);
            this.confi.closeModal();
            this.confi.ventanaEmergente("Mercancia", "Señor usuario, ha ocurrido un error al guardar producto.");
          }
        )
        }
      }, 700);

    } else{
      this.confi.closeModal();
      this.confi.ventanaEmergente("Mercancia", "Señor usuario, por favor ingrese los campos obligatorios.");
    }
  }

        /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de listar el producto
  * @param: producto este es el objeto a listar en el formulario
  **/
  listarProducto(producto:ProductosDto){
    this.confi.loader();
    setTimeout(() => {
      this.confi.closeModal();
      this.formProducto.controls.nombre.setValue(producto.nombre);
      this.formProducto.controls.fechaIngreso.setValue(producto.fecha_ingreso);
      this.formProducto.controls.cantidad.setValue(producto.cantidad);
      this.idProducto = producto.id;
      this.isEditarProducto = true;
    }, 700);
  }


        /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de limpiar el formulario e inicializar las variables
  * @param: formProducto el cual se usa para limpiar el formulario
  **/
  limpiarForma(formProducto:FormGroup){
    formProducto.reset();
    this.idProducto = 0;
    this.isEditarProducto = false;
    this.productos =[];
    this.dataSource = new MatTableDataSource(this.productos);
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 700);
  }


          /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de validar los campos
  * @param: no tiene parametros
  **/
  validarProductos(): boolean {
    if ((this.formProducto.controls.nombre.value !== "" &&
      this.formProducto.controls.nombre.value !== null &&
      this.formProducto.controls.nombre.value !== undefined) ||
      (this.formProducto.controls.fechaIngreso.value !== "" &&
        this.formProducto.controls.fechaIngreso.value !== null &&
        this.formProducto.controls.fechaIngreso.value !== undefined)) {
      return true;
    } else {
      this.confi.ventanaEmergente("Mercancia", "Señor usuario, para consultar los productos por favor ingrese los valores para filtrar.");
      return false;
    }
  }

          /** 
  * @author:Henry Garcia
  * @description: Funcion encargada de validar los campos
  * @param: producto este es el objeto a eliminar
  **/
  eliminarProducto(producto:ProductosDto){
    this.confi.loader();
      setTimeout(() => {
        console.log('producto', producto);
        this.productoService.eliminarProducto(producto).subscribe(
          (respuesta:GenericDto)=>{
            this.confi.closeModal();
            if (respuesta.status !== "500") {
              this.confi.ventanaEmergente("Mercancia", "Señor usuario, "+respuesta.payload);  
              this.obtenerProductos();   
            } else{
              this.confi.ventanaEmergente("Mercancia", "Señor usuario, "+respuesta.payload);     
            }
          }, (error: any) =>{
            console.log('error', error);
            this.confi.closeModal();
            this.confi.ventanaEmergente("Mercancia", "Señor usuario, ha ocurrido un error al eliminar producto.");
          }
        )
      }, 700);
    
  }


}
