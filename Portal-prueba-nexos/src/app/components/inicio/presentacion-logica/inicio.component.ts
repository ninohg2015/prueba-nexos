import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'ng2-ui-auth';
import { Configurador } from 'src/app/core/services/configuradorPortal';
import { GenericDto } from '../../model/GenericDto';

@Component({
  selector: 'inicio-app',
  templateUrl: '../presentacion/inicio.component.html',
  styleUrls: ['../presentacion/inicio.component.css']
})

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar la logica de inicio
 **/
export class InicioComponent {
  title = 'Portal Cautivo';
  hide = false;
  vista = 'login';
  noLoading = true;
  imgLogo = '/assets/img/icons/aquiles.png';
  imgOlvido = '/assets/img/pictures/olvido.png';

  userid: string="";
  userdoc = '';
  userpass = '';
  roles: any;
  formCliente=new FormGroup({
    usuario: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required)
});
  tiposDocumentos=[]

  constructor(private auth: AuthService,public confi:Configurador){
    localStorage.clear();
  }



    /** 
 * @author:Henry Garcia
 * @description: Funcion encargada de realizar la logica para el inicio sesion
 **/
  iniciarSesion(){

    let usuarioLogin = {
      nombre:this.formCliente.controls.usuario.value,
      password:this.formCliente.controls.password.value
    };
    this.confi.loader();
    this.auth.login(usuarioLogin)
    .subscribe((respuestaLogin: GenericDto) => {
    if (respuestaLogin.status === '200') {
      setTimeout(() => {
      this.confi.closeModal();
      this.auth.setToken(respuestaLogin.payload);
      this.auth.isAuthenticated();
      console.log('this.auth.getPayload()', this.auth.getPayload().sub);
      this.confi.setLocal("idUsuario",this.auth.getPayload().sub);
      this.confi.ir(['/home'])
    }, 1000);

    }else{
      this.confi.closeModal();
      localStorage.clear();
      this.auth.logout();
      sessionStorage.clear();
      this.confi.ventanaEmergente("Autenticacion fallida","Señor usuario, usuario o contraseña incorrectos.");
    }

    }, err => {
      this.confi.closeModal();
      this.confi.ventanaEmergente("Error servicio","Señor usuario, ocurrio un error al hacer porceso de logueo por favor intente nuevamente mas tarde.");

    });
  }




}
