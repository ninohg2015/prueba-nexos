import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "ng2-ui-auth";
import { CustomMaterialModule } from "src/app/core/material-module";
import { InicioComponent } from "./inicio.component";
import { InicioRoutingModule } from "./inicio.routing.module";
import { InicioService } from "./inicio.service";

@NgModule({
  declarations: [
    InicioComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    InicioRoutingModule,
    HttpClientModule
  ],
  providers: [
    InicioService
  ],
  bootstrap: [InicioComponent],
  exports: [
    InicioComponent
  ]
})
export class InicioModule { }
