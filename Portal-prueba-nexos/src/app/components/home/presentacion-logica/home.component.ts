import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'ng2-ui-auth';
import { Configurador } from 'src/app/core/services/configuradorPortal';

@Component({
  selector: 'home-app',
  templateUrl: '../presentacion/home.component.html',
  styleUrls: ['../presentacion/home.component.css']
})

/** 
 * @author:Henry Garcia
 * @description: Clase encargada de gestionar la logica del home
 **/
export class HomeComponent {
  hide = false;
  vista = 'login';
  nitEmpresa="";
  noLoading = true;
  tiposDocumentos=[]

  constructor(public confi:Configurador){
  }
  


}
