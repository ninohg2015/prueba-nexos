import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "ng2-ui-auth";
import { CustomMaterialModule } from "src/app/core/material-module";
import { InicioService } from "../../inicio/presentacion-logica/inicio.service";
import { HomeComponent } from "./home.component";
import { HomeRoutingModule } from "./home.routing.module";


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    HttpClientModule
  ],
  providers: [
    InicioService
  ],
  bootstrap: [HomeComponent],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
