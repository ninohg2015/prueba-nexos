package co.com.nexos.prueba.controller;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.nexos.prueba.dto.ConsultaDto;
import co.com.nexos.prueba.dto.GenericDto;
import co.com.nexos.prueba.dto.LoginDto;
import co.com.nexos.prueba.dto.ProductoDto;
import co.com.nexos.prueba.dto.UsuarioDto;
import co.com.nexos.prueba.iservices.IMercanciaService;
import lombok.extern.log4j.Log4j2;

/******************************************************************************
 * @autor: Henry Garcia
 * @fecha: 19/06/2021
 * @descripción: Clase contenedora de los controladores
 ******************************************************************************/
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
@Log4j2
public class MercanciaController {

    
    
    private IMercanciaService procesoService;

    @Autowired
    public MercanciaController(
    		IMercanciaService procesoService) {
    	this.procesoService = procesoService;
    }

	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: consultarProductos
	 * @descripcion: METODO ENCARGADO DE OBTENER TODOS LOS PRODUCTOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/consultar/consultarProductos")
    public GenericDto consultarProductos(){
    	return procesoService.obtenerProductos();
    }
    
	/**************************************************************************************
	 * @param ConsultaDto consulta
	 * @funcion: consultarProductosFiltro
	 * @descripcion: METODO ENCARGADO DE CONSULTAR LISTADO PRODUCTOS EN BASE A LOS FILTROS RECIBIDOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/consultar/consultarProductosFiltro")
    public GenericDto consultarProductosFiltro(@RequestBody  ConsultaDto consulta){
    	return procesoService.obtenerProductosPorFiltro(consulta);
    }
    
	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: obtenerCargos
	 * @descripcion: METODO ENCARGADO DE OBTENER LOS CARGOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/consultar/obtenerCargos")
    public GenericDto obtenerCargos() {
    	return procesoService.obtenerCargos();
    }

	/**************************************************************************************
	 * @param LoginDto login
	 * @funcion: login
	 * @descripcion: METODO ENCARGADO DE REALIZAR LA LOGICA DE INICIO DE SESION
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/usuario/login")
    public GenericDto login(@RequestBody  LoginDto login) {
    	return procesoService.login(login.getNombre(),login.getPassword());
    }
    
	/**************************************************************************************
	 * @param UsuarioDto usuario
	 * @funcion: guardarUsuario
	 * @descripcion: METODO ENCARGADO DE CREAR O ACTUALIZAR UN USUARIO
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/usuario/crearUsuario")
    public GenericDto guardarUsuario(@RequestBody  UsuarioDto usuario) {
    	return procesoService.guardarUsuario(usuario);
    }
    
	/**************************************************************************************
	 * @param ProductoDto producto
	 * @funcion: guardarProducto
	 * @descripcion: METODO ENCARGADO DE CREAR O ACTUALIZAR UN PRODUCTO
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/producto/crearProducto")
    public GenericDto guardarProducto(@RequestBody  ProductoDto producto) {
    	return procesoService.guardarProducto(producto);
    }
    
	/**************************************************************************************
	 * @param ProductoDto producto
	 * @funcion: eliminarProducto
	 * @descripcion: METODO ENCARGADO DE ELIMINAR PRODUCTO
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/producto/eliminarProducto")
    public GenericDto eliminarProducto(@RequestBody  ProductoDto producto) {
    	return procesoService.eliminarProducto(producto);
    }
    
    
	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: consultarUsuarios
	 * @descripcion: METODO ENCARGADO DE OBTENER TODOS LOS USUARIOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/consultar/consultarUsuarios")
    public GenericDto consultarUsuarios(){
    	return procesoService.obtenerUsuarios();
    }
    
	/**************************************************************************************
	 * @param UsuarioDto usuarioDto
	 * @funcion: eliminarUsuario
	 * @descripcion: METODO ENCARGADO DE ELIMINAR UN USUARIO
	 * @autor: Henry Garcia
	 **************************************************************************************/
    @PostMapping(value = "/usuario/eliminarUsuario")
    public GenericDto eliminarUsuario(@RequestBody  UsuarioDto usuarioDto){
    	return procesoService.eliminarUsuario(usuarioDto);
    }


    

}