package co.com.nexos.prueba.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the "usuarios" database table.
 * 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="usuarios", schema = "prueba")
@NamedQueries({ @NamedQuery(name="Usuarios.findAll", query="SELECT u FROM Usuarios u"),
				@NamedQuery(name="Usuarios.findForName", query="SELECT u FROM Usuarios u where u.nombre = :nombre")})
public class Usuarios implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SEC_USUARIOS_GENERATOR", sequenceName = "prueba.usuarios_id_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEC_USUARIOS_GENERATOR")
	@Column(name="id")
	private int id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="password")
	private String password;

	@Column(name="cargo")
	private int cargo;
	
	@Column(name="fecha_ingreso")
	private Date fecha_ingreso;
	
	@Column(name="edad")
	private int edad;

}