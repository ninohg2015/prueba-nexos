package co.com.nexos.prueba.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductoDto {

	private Date fecha_ingreso;
	private String nombre;
	private int cantidad;
	private int usuario;
	private int id;

	

}
