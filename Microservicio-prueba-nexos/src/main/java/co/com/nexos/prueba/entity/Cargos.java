package co.com.nexos.prueba.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the "tpo_ejcucion_prcsos" database table.
 * 
 */
@Data
@NoArgsConstructor
@Entity
@Table(name="cargos", schema = "prueba")
@NamedQuery(name="Cargos.findAll", query="SELECT c FROM Cargos c")
public class Cargos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Column(name="nombre_cargo")
	private String nombre_cargo;
	
	@Column(name="descripcion_cargo")
	private String descripcion_cargo;



}