/*
 * Redcolsa RedColombiana De Servicios S.A.
 * Copyright 2020-2030 Redcolsa, Todos los derechos reservados.         
 */

package co.com.nexos.prueba.services;

import org.springframework.stereotype.Service;

import co.com.nexos.prueba.iservices.IUtilidadesService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/******************************************************************************
 * @autor: Diego Fernando Guerrero
 * @fecha: 31/08/2020
 * @descripción: Clase utilidades la cual contiene los metodos que no hacen parte
 * 	de la logica de negocio.
 * @copyright:  2020-2030 Redcolsa, Todos los derechos reservados.  
 ******************************************************************************/
@Service
public class UtilidadesService implements IUtilidadesService {

    @Override
    public boolean esValorNumerico(String pCadena) {
        boolean salida = false;

        try {
            Double.parseDouble(pCadena);
            salida=true;
        }catch (Exception e){

        }
        return salida;
    }

    @Override
    public boolean formatoFechaCorrecto(String pFormatoFecha, String pCadenaFecha) {
        boolean formatoCorrecto=false;

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pFormatoFecha);
            @SuppressWarnings("unused")
			Date fechaCadena = simpleDateFormat.parse(pCadenaFecha);
            formatoCorrecto = true;
        }catch (Exception e){

        }
        return formatoCorrecto;
    }


    @Override
    public Date obtenerFechaDeCadena(String pCadenaFecha) {
       Date fechaSalida=null;
        try{
           SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
           return simpleDateFormat.parse(pCadenaFecha);
       }catch (Exception e){

       }
        return fechaSalida;
    }
    
    @Override
    public int generarID() {
    	return (int) System.currentTimeMillis();
    }
    
    @Override
    public String createAndWrite(String namePath, String nameFile, String data) {
    	
    	String response = new String();
    	
		try {
			
			BufferedWriter bufferedW = null;
	        FileWriter fileW = null;
            
            String fichero = namePath + nameFile;
    		File file = new File(fichero);
    		
    		if (!file.exists()) {
    			file.createNewFile();
			}
    		
    		fileW = new FileWriter(file, true);
    		bufferedW = new BufferedWriter(fileW);
    		bufferedW.write(data);
    		bufferedW.newLine();
    		bufferedW.close();
            
            response = nameFile;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return response;

	}
    
}
