/*
 * Redcolsa RedColombiana De Servicios S.A.
 * Copyright 2020-2030 Redcolsa, Todos los derechos reservados.         
 */

package co.com.nexos.prueba.iservices;

import java.util.Date;

/******************************************************************************
 * @autor: Diego Fernando Guerrero
 * @fecha: 31/08/2020
 * @descripción: Interfaz que contiene los metodos publicos de la clase 
 * 	UtilidadesService
 * @copyright:  2020-2030 Redcolsa, Todos los derechos reservados.  
 ******************************************************************************/
public interface IUtilidadesService {
	
    public boolean esValorNumerico(String pCadena);

    public boolean formatoFechaCorrecto(String pFormatoFecha, String pCadenaFecha);

    public Date obtenerFechaDeCadena(String pCadenaFecha);
    
    public int generarID();
    
    public String createAndWrite(String namePath, String nameFile, String data);
}
