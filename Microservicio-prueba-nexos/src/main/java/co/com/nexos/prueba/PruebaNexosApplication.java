package co.com.nexos.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/******************************************************************************
 * @autor: Henry Garcia
 * @fecha: 19/06/2021
 * @descripción: Clase contenedora del metodo principal
 ******************************************************************************/
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages= {"co.com.nexos.prueba", "co.com.nexos.prueba.repository"})
@EntityScan(basePackages= {"co.com.nexos.prueba.entity"})
public class PruebaNexosApplication {

	/**************************************************************************************
	 * @param args
	 * @funcion: main
	 * @descripcion: Metodo principal de la aplicacion
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public static void main(String[] args) {
		SpringApplication.run(PruebaNexosApplication.class, args);
	}
	
	@Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

}