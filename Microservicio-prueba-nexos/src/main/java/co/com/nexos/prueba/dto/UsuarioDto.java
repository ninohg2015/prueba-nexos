package co.com.nexos.prueba.dto;



import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UsuarioDto {
	private int id;	
	private String nombre;	
	private String password;
	private int cargo;
	private Date fecha_ingreso;
	private int edad;

}
