package co.com.nexos.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;



@Entity
@Table(name="productos", schema = "prueba")
@NamedQueries({
	@NamedQuery(name="Productos.findAll", query="SELECT p FROM Productos p"),
	@NamedQuery(name="Productos.findForName", query="SELECT p FROM Productos p where p.nombre = :nombre"),
	})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Productos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SEC_PRODUCTOS_GENERATOR", sequenceName = "prueba.productos_id_seq", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SEC_PRODUCTOS_GENERATOR")
	@Column(name="id")
	private int id;

	@Getter
	@Column(name="nombre")
	private String nombre;

	@Column(name="cantidad")
	private int cantidad;
	
	@Column(name="fecha_ingreso")
	private Date fecha_ingreso;
	
	@Column(name="usuario")
	private int usuario;

	
	


}