package co.com.nexos.prueba.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConsultaDto {

	private Date fecha_ingreso;
	private String nombre;	

}
