package co.com.nexos.prueba.irepository;

import java.util.List;
import java.util.Map;


public interface IDaoGenerico {

	public <T> T guardar(T entidad);
	public <T> T actualizar(T entidad);
	public <T> boolean eliminar(T entidad);
	public <T> T buscarPorId(Class<T> ClaseEntidad, int id);
	public <T> List<T> buscarTodosLosRegistros(Class<T> claseEntidad);
	public <T> List<T> buscarRegistrosPorNamedQuery(Class<T> claseEntidad, String namedQuery, Map<String, Object> parametros);
	public <T> T buscarUnSoloRegistroPorNamedQuery(Class<T> claseEntidad, String namedQuery, Map<String, Object> parametros);
	public List<Object> ejecutarConsultaNativa(String namedQuery, Map<String, Object> parametros);
	public List<Object> ejecutarProcedimiento(String namedQuery, Map<String, Object> parametros);
	public Object ejecutarConsultaNativaObjeto(String namedQuery, Map<String, Object> parametros);
}
