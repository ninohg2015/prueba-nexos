package co.com.nexos.prueba.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.nexos.prueba.auth.JwtProvider;
import co.com.nexos.prueba.dto.ConsultaDto;
import co.com.nexos.prueba.dto.GenericDto;
import co.com.nexos.prueba.dto.ProductoDto;
import co.com.nexos.prueba.dto.UsuarioDto;
import co.com.nexos.prueba.entity.Productos;
import co.com.nexos.prueba.entity.Usuarios;
import co.com.nexos.prueba.entity.Cargos;
import co.com.nexos.prueba.irepository.IDaoGenerico;
import co.com.nexos.prueba.iservices.IMercanciaService;

@Service
public class MercanciaService implements IMercanciaService {

	
	private Logger logger = LoggerFactory.getLogger("logger");	
	private IDaoGenerico daoGenerico;
	public ObjectMapper mapper;
	
	@Autowired
	JwtProvider jwtProvider;
	
	@Autowired
	AuthenticationManager authenticationManager;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	public MercanciaService(IDaoGenerico daoGenerico) {
		this.daoGenerico = daoGenerico;
		mapper = new ObjectMapper();
	}
	
	
	
	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: obtenerProductos
	 * @descripcion: METODO ENCARGADO DE OBTENER LOS PRODUCTOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto obtenerProductos() {
		logger.info("Ingresando en metodo obtenerProductos");
		try {
			List<Productos> listadoProductos = daoGenerico.buscarTodosLosRegistros(Productos.class);
			logger.info("Respuesta de listado de productos: "+listadoProductos);
			return GenericDto.sucess(listadoProductos);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerProductos : "+e.getMessage());
			return  GenericDto.failed(e.getMessage());
		}
	}
	
	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: obtenerUsuarios
	 * @descripcion: METODO ENCARGADO DE OBTENER EL LISTADO DE USUARIOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto obtenerUsuarios() {
		logger.info("Ingresando en metodo obtenerUsuarios");
		try {
			List<Usuarios> listadoUsuarios = daoGenerico.buscarTodosLosRegistros(Usuarios.class);
			logger.info("Respuesta de listado de usuarios: "+listadoUsuarios);
			return GenericDto.sucess(listadoUsuarios);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerUsuarios : "+e.getMessage());
			return  GenericDto.failed(e.getMessage());
		}
	}
	
	/**************************************************************************************
	 * @param NINGUNO
	 * @funcion: obtenerCargos
	 * @descripcion: METODO ENCARGADO DE OBTENER EL LISTADO DE CARGOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto obtenerCargos() {
		logger.info("Ingresando en metodo obtenerCargos");
		try {
			List<Cargos> listadoCargos = daoGenerico.buscarTodosLosRegistros(Cargos.class);
			logger.info("Respuesta de listado de tipo ejecucion procesos: "+listadoCargos);
			return GenericDto.sucess(listadoCargos);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerCargos : "+e.getMessage());
			return  GenericDto.failed(e.getMessage());
		}

	}
	
	
	/**************************************************************************************
	 * @param String nombre, String clave
	 * @funcion: login
	 * @descripcion: METODO ENCARGADO DE REALIZAR LA LOGICA DE INICIO DE SESION
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto login(String nombre, String clave) {
		
		try {
			logger.info("Ingresando al metodo login");
			Usuarios usuarioLogin = obtenerUsuarioPorNombre(nombre);
			if (null == usuarioLogin) {
				logger.error("No se encontro usuario con el nombre : "+nombre);
				return  GenericDto.failed("No se encontro usuario con el nombre : "+nombre);
			} else {
						Authentication authentication = null;
						authentication = authenticationManager
								.authenticate(new UsernamePasswordAuthenticationToken(nombre, clave));
						SecurityContextHolder.getContext().setAuthentication(authentication);
						String jwt = jwtProvider.generateJwtToken(authentication);		
						return GenericDto.sucess(jwt);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error al ejecutar metodo login : "+e.getMessage());
			return  GenericDto.failed(e.getMessage());
		}
	}
	
	
	/**************************************************************************************
	 * @param String nombre
	 * @funcion: obtenerUsuarioPorNombre
	 * @descripcion: METODO ENCARGADO DE CONSULTAR EL USUARIO POR EL NOMBRE
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public Usuarios obtenerUsuarioPorNombre(String nombre) {
		Usuarios usuarioLogin = null;
		logger.info("Ingresando al metodo obtenerUsuarioPorNombre");
		try {
			Map<String, Object> parametrosConsulta = new LinkedHashMap<String, Object>();
			parametrosConsulta.put("nombre", nombre);
			usuarioLogin = daoGenerico.buscarUnSoloRegistroPorNamedQuery(Usuarios.class, "Usuarios.findForName", parametrosConsulta);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerUsuarioPorNombre : "+e.getMessage());
		}
		return usuarioLogin;
	}
	

	/**************************************************************************************
	 * @param ConsultaDto consulta
	 * @funcion: obtenerProductosPorFiltro
	 * @descripcion: METODO ENCARGADO DE CONSULTAR LISTADO PRODUCTOS EN BASE A LOS FILTROS RECIBIDOS
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto obtenerProductosPorFiltro(ConsultaDto consulta) {
		List<Productos> productos = null;
		logger.info("Ingresando al metodo obtenerProductosPorFiltro");
		try {
			StringBuilder consultaSql = new StringBuilder();
			consultaSql.append("Select p.id, p.nombre, p.cantidad, p.fecha_ingreso, p.usuario from prueba.productos p where ");
			consultaSql.append(consulta.getNombre() != null?"p.nombre like :nombre ":"");
			consultaSql.append(consulta.getNombre() != null && consulta.getFecha_ingreso() != null ?" and ":"");
			consultaSql.append(consulta.getFecha_ingreso() != null?"p.fecha_ingreso = :fechaIngreso":"");
			logger.info("Consulta final : "+consultaSql.toString());

			Map<String, Object> parametrosConsulta = new LinkedHashMap<String, Object>();
			if (consulta.getFecha_ingreso() != null) {
				parametrosConsulta.put("fechaIngreso", consulta.getFecha_ingreso());
			}
			if (consulta.getNombre() != null) {
				parametrosConsulta.put("nombre", "%"+consulta.getNombre()+"%");

			}
			List<Object> respuesta = daoGenerico.ejecutarConsultaNativa(consultaSql.toString(), parametrosConsulta);
			productos = cast(respuesta);
			return GenericDto.sucess(productos);
		} catch (Exception e) {
			logger.error("Error al ejecutar metodo obtenerProductosPorFiltro : "+e.getMessage());
			return  GenericDto.failed("Error al ejecutar metodo obtenerProductosPorFiltro : "+e.getMessage());
		}
	}
	
	
	/**************************************************************************************
	 * @param int id
	 * @funcion: obtenerUsuarioPorId
	 * @descripcion: METODO ENCARGADO DE CONSULTAR EL USUARIO POR EL IDENTIFICADOR
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public Usuarios obtenerUsuarioPorId(int id) {
		Usuarios usuarioLogin = null;
		logger.info("Ingresando al metodo obtenerUsuarioPorId");
		try {
			usuarioLogin = daoGenerico.buscarPorId(Usuarios.class, id);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerUsuarioPorId : "+e.getMessage());
		}
		return usuarioLogin;
	}
	
	
	/**************************************************************************************
	 * @param int id
	 * @funcion: obtenerProductosPorId
	 * @descripcion: METODO ENCARGADO DE CONSULTAR EL PRODUCTO POR EL IDENTIFICADOR
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public Productos obtenerProductosPorId(int id) {
		Productos producto = null;
		logger.info("Ingresando al metodo obtenerProductosPorId");
		try {
			producto = daoGenerico.buscarPorId(Productos.class, id);
		} catch (Exception e) {
			logger.error("Error al ejecutar metodo obtenerProductosPorId : "+e.getMessage());
		}
		return producto;
	}
	
	
	/**************************************************************************************
	 * @param String nombre
	 * @funcion: obtenerProductoPorNombre
	 * @descripcion: METODO ENCARGADO DE CONSULTAR EL PRODUCTO POR EL NOMBRE
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public Productos obtenerProductoPorNombre(String nombre) {
		Productos producto = null;
		logger.info("Ingresando al metodo obtenerProductoPorNombre");
		try {
			Map<String, Object> parametrosConsulta = new LinkedHashMap<String, Object>();
			parametrosConsulta.put("nombre", nombre);
			producto = daoGenerico.buscarUnSoloRegistroPorNamedQuery(Productos.class, "Productos.findForName", parametrosConsulta);
		} catch (Exception e) {
			logger.info("Error al ejecutar metodo obtenerProductoPorNombre : "+e.getMessage());
		}
		return producto;
	}
	
	/**************************************************************************************
	 * @param Collection<?> list
	 * @funcion: cast
	 * @descripcion: METODO ENCARGADO DE CASTEAR EL List<Object> A List<Productos>
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public List<Productos> cast(Collection<?> list) {
	    List<Productos> valueList = new ArrayList<Productos>(list.size());

	    for(Object o : list) {
	        // throws casting exception
	    	Object[] objeto = (Object[]) o;
	    	Productos producto = new Productos();
	    	BigDecimal cantidad = (BigDecimal) objeto[2];

	    	producto.setCantidad(cantidad.intValue());
	    	producto.setNombre((String) objeto[1]);
	    	producto.setFecha_ingreso((Date) objeto[3]);
	    	producto.setUsuario((int) objeto[4]);
	    	producto.setId((int) objeto[0]);
	        valueList.add(producto);  
	    }
	    return valueList;
	}
	
	
	/**************************************************************************************
	 * @param UsuarioDto usuarioDto
	 * @funcion: guardarUsuario
	 * @descripcion: METODO ENCARGADO DE CREAR O ACTUALIZAR AL USUARIO
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto guardarUsuario(UsuarioDto usuarioDto) {
		Usuarios usuario = null;
		try {
			if (!this.validarNombreUsuario(usuarioDto)) {
				return GenericDto
						.failed("El nombre del usuario " + usuarioDto.getNombre() + " ya se encuentra en uso.");
			} else {
			if (usuarioDto.getId() != 0) {
				 usuario = new Usuarios();
				 	usuario.setId(usuarioDto.getId());
					usuario.setCargo(usuarioDto.getCargo());
					usuario.setNombre(usuarioDto.getNombre());
					usuario.setPassword(encoder.encode(usuarioDto.getPassword()));
					usuario.setFecha_ingreso(usuarioDto.getFecha_ingreso());
					usuario.setEdad(usuarioDto.getEdad());
					daoGenerico.actualizar(usuario);
					return GenericDto.sucess("El usuario se ha actualizado de manera exitosa");
			}else {

					usuario = new Usuarios();
					usuario.setCargo(usuarioDto.getCargo());
					usuario.setNombre(usuarioDto.getNombre());
					usuario.setPassword(encoder.encode(usuarioDto.getPassword()));
					usuario.setFecha_ingreso(usuarioDto.getFecha_ingreso());
					usuario.setEdad(usuarioDto.getEdad());
					daoGenerico.guardar(usuario);
					return GenericDto.sucess("El usuario se ha creado de manera exitosa");
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error al ejecutar metodo guardarUsuario : "+e.getMessage());
			return  GenericDto.failed(e.getMessage());
		}
	}
	
	
	/**************************************************************************************
	 * @param ProductoDto productoDto
	 * @funcion: guardarProducto
	 * @descripcion: METODO ENCARGADO DE CREAR O ACTUALIZAR UN PRODUCTO
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto guardarProducto(ProductoDto productoDto) {
		Productos producto = null;
		try {

			if (!this.validarNombreProducto(productoDto)) {
				return GenericDto
						.failed("El nombre del producto " + productoDto.getNombre() + " ya se encuentra en uso.");
			} else {
				if (productoDto.getId() != 0) {
					producto = this.obtenerProductosPorId(productoDto.getId());
					producto.setFecha_ingreso(productoDto.getFecha_ingreso());
					producto.setNombre(productoDto.getNombre());
					producto.setCantidad(productoDto.getCantidad());
					daoGenerico.guardar(producto);
					return GenericDto.sucess("El producto se ha actualizado de manera exitosa");
				} else {

					producto = new Productos();
					producto.setFecha_ingreso(productoDto.getFecha_ingreso());
					producto.setNombre(productoDto.getNombre());
					producto.setCantidad(productoDto.getCantidad());
					producto.setUsuario(productoDto.getUsuario());
					daoGenerico.guardar(producto);
					return GenericDto.sucess("El producto se ha creado de manera exitosa");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error al ejecutar metodo guardarUsuario : " + e.getMessage());
			return GenericDto.failed(e.getMessage());
		}
	}
	
	/**************************************************************************************
	 * @param ProductoDto productoDto
	 * @funcion: eliminarProducto
	 * @descripcion: METODO ENCARGADO DE ELIMINAR UN PRODUCTO
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto eliminarProducto(ProductoDto productoDto) {
		Productos producto = null;
		try {
		
				if (productoDto.getId() != 0) {
					producto = this.obtenerProductosPorId(productoDto.getId());
					producto.setFecha_ingreso(productoDto.getFecha_ingreso());
					producto.setNombre(productoDto.getNombre());
					producto.setCantidad(productoDto.getCantidad());
					daoGenerico.eliminar(producto);
					return GenericDto.sucess("El producto se ha eliminado de manera exitosa");
				} else {
					return GenericDto.failed("El id del producto enviado no es valido.");
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error al ejecutar metodo eliminarProducto : " + e.getMessage());
			return GenericDto.failed(e.getMessage());
		}
	}
	
	/**************************************************************************************
	 * @param UsuarioDto usuarioDto
	 * @funcion: eliminarUsuario
	 * @descripcion: METODO ENCARGADO DE ELIMINAR UN USUARIO
	 * @autor: Henry Garcia
	 **************************************************************************************/
	@Override
	public GenericDto eliminarUsuario(UsuarioDto usuarioDto) {
		Usuarios usuario = null;
		try {
		
				if (usuarioDto.getId() != 0) {
					usuario = this.obtenerUsuarioPorId(usuarioDto.getId());
					daoGenerico.eliminar(usuario);
					return GenericDto.sucess("El usuario se ha eliminado de manera exitosa");
				} else {
					return GenericDto.failed("El id del usuario enviado no es valido.");
				}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Error al ejecutar metodo eliminarUsuario : " + e.getMessage());
			return GenericDto.failed(e.getMessage());
		}
	}
	
	/**************************************************************************************
	 * @param ProductoDto productoDto
	 * @funcion: validarNombreProducto
	 * @descripcion: METODO ENCARGADO DE VALIDAR QUE EL NOMBRE DEL PRODUCTO ESTE DISPONIBLE
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public Boolean validarNombreProducto(ProductoDto productoDto) {
		
		try {
			Productos producto = obtenerProductoPorNombre(productoDto.getNombre());
			if (producto != null && producto.getId() != productoDto.getId()) {
			return false;	
			}
			return true;

		} catch (Exception e) {
			logger.info("Error al ejecutar metodo validarNombreProducto : "+e.getMessage());
			return false;
		}
	}
	
	
	/**************************************************************************************
	 * @param UsuarioDto usuarioDto
	 * @funcion: validarNombreUsuario
	 * @descripcion: METODO ENCARGADO DE VALIDAR QUE EL NOMBRE DEL USUARIO ESTE DISPONIBLE
	 * @autor: Henry Garcia
	 **************************************************************************************/
	public Boolean validarNombreUsuario(UsuarioDto usuarioDto) {
		
		try {
			Usuarios usuario = obtenerUsuarioPorNombre(usuarioDto.getNombre());
			if (usuario != null && usuario.getId() != usuarioDto.getId()) {
			return false;	
			}
			return true;

		} catch (Exception e) {
			logger.info("Error al ejecutar metodo validarNombreUsuario : "+e.getMessage());
			return false;
		}
	}
	

	
	
}
