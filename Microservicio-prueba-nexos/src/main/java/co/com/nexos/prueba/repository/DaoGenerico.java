package co.com.nexos.prueba.repository;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import co.com.nexos.prueba.irepository.IDaoGenerico;





/**************************************************************************************
 * @descripcion: CLASE  DAO GENERICO ENCARGADA DE LOGICA GENERAL DE BASE DE DATOS
 * @autor: Henry Garcia
 **************************************************************************************/
@Repository("daoGenericoNexos")
public class DaoGenerico implements IDaoGenerico {
    
	@PersistenceContext
    private EntityManager entityManager;
	
	
	@Override
	@Transactional
	public <T> T guardar(T entidad) {
        entityManager.persist(entidad);
        return entidad;
    }

	@Override
	@Transactional
    public <T> T actualizar(T entidad) {
        entityManager.merge(entidad);
        return entidad;
    }

	@Override
	@Transactional
    public <T> boolean eliminar(T entidad) {
        
    	boolean isDelete = false;
        try{
             entityManager.remove(entityManager.contains(entidad) ? entidad : entityManager.merge(entidad));
             isDelete = true;
        }
        catch(PersistenceException ex){
        	//LogsManager.getInstance().severe("Error al eliminar la entidad "+entidad.toString()+"\n"+ex.getMessage());
        }
        
        return isDelete;
    }

	@Override
    public <T> T buscarPorId(Class<T> ClaseEntidad, int id) {
       return entityManager.find(ClaseEntidad, id);
    }
	
	
	@Override
    @SuppressWarnings("unchecked")
	public <T> List<T> buscarTodosLosRegistros(Class<T> claseEntidad) {
        List<T> listEntities = entityManager.createQuery("Select modelo From "+claseEntidad.getName()+" modelo").getResultList();
        return listEntities;
    }

	@Override
    @SuppressWarnings("unchecked")
	public <T> List<T> buscarRegistrosPorNamedQuery(Class<T> claseEntidad, String namedQuery, Map<String, Object> parametros) {
        Query query = entityManager.createNamedQuery(namedQuery, claseEntidad);
        
        for (Map.Entry<String, Object> entrySet : parametros.entrySet()) {
            String key = entrySet.getKey();
            Object value = entrySet.getValue();
            query.setParameter(key, value);
        }
        return query.getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
	public <T> T buscarUnSoloRegistroPorNamedQuery(Class<T> claseEntidad, String namedQuery, Map<String, Object> parametros) {
    	Object respuesta = null;
        try {
            Query query = entityManager.createNamedQuery(namedQuery, claseEntidad);
            for (Map.Entry<String, Object> entrySet : parametros.entrySet()) {
                String key = entrySet.getKey();
                Object value = entrySet.getValue();
                query.setParameter(key, value);
            }
                respuesta = query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return respuesta != null ? (T) respuesta :null;
    }
    
    
    @Override
    @SuppressWarnings("unchecked")
	public List<Object> ejecutarConsultaNativa(String namedQuery, Map<String, Object> parametros) {
     
    	try {
        	Query query = entityManager.createNativeQuery(namedQuery);
            for (Map.Entry<String, Object> entrySet : parametros.entrySet()) {
                String key = entrySet.getKey();
                Object value = entrySet.getValue();
                query.setParameter(key, value);
            }
            return  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

    }
    
    @Override
    @SuppressWarnings("unchecked")
	public Object ejecutarConsultaNativaObjeto(String namedQuery, Map<String, Object> parametros) {
     
    	try {
        	Query query = entityManager.createNativeQuery(namedQuery);
            for (Map.Entry<String, Object> entrySet : parametros.entrySet()) {
                String key = entrySet.getKey();
                Object value = entrySet.getValue();
                query.setParameter(key, value);
            }
            return  query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

    }
    
    @Override
    @SuppressWarnings("unchecked")    
    public List<Object> ejecutarProcedimiento(String namedQuery, Map<String, Object> parametros){
    	try {
        	Query query = entityManager.createNativeQuery(namedQuery);
            for (Map.Entry<String, Object> entrySet : parametros.entrySet()) {
                String key = entrySet.getKey();
                Object value = entrySet.getValue();
                query.setParameter(key, value);
            }
            return  query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    }
	
    
}
