package co.com.nexos.prueba.auth;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.nexos.prueba.entity.Usuarios;
import co.com.nexos.prueba.iservices.IMercanciaService;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
  @Autowired
  IMercanciaService mercanciaService;
 
  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws NoResultException {
 
    Usuarios user;
	try {
		user = mercanciaService.obtenerUsuarioPorNombre(username);
	} catch (Exception e) {
		user = null;
	}  
 
    return UserPrinciple.build(user);
  }
}
