package co.com.nexos.prueba.iservices;

import co.com.nexos.prueba.dto.ConsultaDto;
import co.com.nexos.prueba.dto.GenericDto;
import co.com.nexos.prueba.dto.ProductoDto;
import co.com.nexos.prueba.dto.UsuarioDto;
import co.com.nexos.prueba.entity.Usuarios;

public interface IMercanciaService {
	public GenericDto obtenerProductos();
	public GenericDto obtenerCargos();
	public Usuarios obtenerUsuarioPorNombre(String nombre);
	public GenericDto login(String nombre, String clave);
	public GenericDto guardarUsuario(UsuarioDto usuarioDto);
	public GenericDto obtenerProductosPorFiltro(ConsultaDto consulta);
	public GenericDto guardarProducto(ProductoDto productoDto);
	public GenericDto eliminarProducto(ProductoDto productoDto);
	public GenericDto obtenerUsuarios();
	public GenericDto eliminarUsuario(UsuarioDto usuarioDto);
}
